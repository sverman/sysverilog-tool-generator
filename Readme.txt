This is the generator code.

Use the following steps to run the code. 
Run the Makefile using the command - make
run the toolgenerator using the parameters p,b,g in the following manner
 
./toolgenerator <p> <b> <g> <filename>  

The file <filename> will be generated without the extension so please include the file extension .sv with the name itself.
