/*
 * toolgen.c
 *
 *  Created on: Mar 1, 2013
 *      Author: sambhav
 */
	#include<stdlib.h>
	#include<stdio.h>
	#include<math.h>

	int main(int argc, char **argv)
	{

	int i,j,k,l,m,n,loop,tb,base;

	printf("\n Welcome to hardware tool generator:\n");
	if (argc!=5)
	{
		printf("\n You didn't enter sufficient inputs (only %d entered).Now exiting... ",argc);
		exit(0);
	}

	int p = atoi(argv[1]);
	int b = atoi(argv[2]);
	int g = atoi(argv[3]);


	tb = pow(2,b-2);		// this value will be used by the random function generator to generate test vectors
	base = pow (2,b-2);		// base value to add to randomly genrated testvectors

	if(p%2!=0 || p==0)
	{
	printf("Parallelism needs to be an even non-zero field. Plz try again. Now exiting...");
	exit(0);
	}

	printf("\nConfiguration of hardware tool generator:\n");
	printf("\nParallelism: %d",p);
	printf("\nBit Precision: %d",b);
	printf("\nPipelining Mode: %d",g);

	printf("\n.\n...\n...\n Now generating source...");

	FILE *outfile;
	if ((outfile = fopen(argv[4], "w+")) == NULL)
	{
      printf("Error: cannot open file %s for writing.\n", argv[4]);
	  exit(0);
	}
	fprintf(outfile,"module mac_%d_%d_%d (clk,reset,f,overflow,",p,b,g);

	for (i=0;i<p;i++)
	{
	fprintf (outfile,"in%d",i);
	if(i!=p-1)
		fprintf(outfile,",");
	}
	fprintf(outfile,");");
	fprintf(outfile,"\ninput clk, reset;");
	fprintf(outfile,"\ninput signed [%d:0]",b-1);
	for (i=0;i<p;i++)
	{
	fprintf(outfile," in%d",i);
	if(i!=p-1)
	fprintf(outfile,",");
	}

	fprintf(outfile,";\n output logic overflow;");
	fprintf(outfile,"\n output logic signed [%d:0] f;",b*2-1);
	// create registers
	//input register begins
	fprintf(outfile,"\n logic signed [%d:0] add_out;",b*2-1);
	fprintf(outfile,"\n logic signed [%d:0] add_out2;",b*2-1);
	//if(g==1)
	//fprintf(outfile,"\n logic signed [%d:0] add_out2reg;",b*2-1);

	if(g==1)
	{
		fprintf(outfile,"\n logic signed [%d:0]",b*2-1);
		for(i=0;i<p/2;i++)
		{
		fprintf(outfile," add_outS%dwire",i);
		if(i!=p/2-1)
			fprintf(outfile,",");
		}
		fprintf(outfile,";");
	}

	if(g==1)
		{
			fprintf(outfile,"\n logic signed [%d:0]",b*2-1);
			for(i=0;i<p/2;i++)
			{
			fprintf(outfile," add_outS%dreg",i);
			if(i!=p/2-1)
				fprintf(outfile,",");

			}
			fprintf(outfile,";");
		}

	fprintf(outfile,"\n logic signed [%d:0] freg;",b*2-1);

	fprintf(outfile,"\n logic signed [%d:0]",b-1);
	for (i=0;i<p;i++)
	{
	fprintf (outfile,"in%dreg",i);
	if(i!=p-1)
	fprintf(outfile,",");
	}
	fprintf(outfile,";");
	//input register ends

	//multiplier register begins
	if (g==0)
	fprintf(outfile,"\n wire signed [%d:0]",b*2-1);
	if(g==1)
	fprintf(outfile,"\n logic signed [%d:0]",b*2-1);
	for (i=0;i<p/2;i++)
		{
		fprintf (outfile,"mul%dreg",i);
		if(i!=p/2-1)
		fprintf(outfile,",");
		}
	fprintf(outfile,";\n");
	if(g==1) //declare wires only if in ultra pipelining mode
	{
	fprintf(outfile," wire signed [%d:0]",b*2-1);
	for (i=0;i<p/2;i++)
		{
		fprintf (outfile,"mul%dwire",i);
		if(i!=p/2-1)
		fprintf(outfile,",");
		}
	fprintf(outfile,";\n");
	}
	//fprintf(outfile,"logic signed add_ovf_reg[%d:0];//register to store result of additions of multiplier stage",n);
	// multiplier register ends
	fprintf(outfile,"\n logic signed ");
		for (i=0;i<p-1;i++)
		{
		fprintf (outfile,"overflowS%d",i);
		if(i!=p-2)
		fprintf(outfile,",");
		}
		fprintf(outfile,";");
		fprintf(outfile,"\n logic signed [%d:0]",2*b-1);
				for (i=0;i<p-1;i++)
				{
				fprintf (outfile,"add_outS%d",i);
				if(i!=p-2)
				fprintf(outfile,",");
				}
				fprintf(outfile,";");

		fprintf(outfile,"\nlogic overflow1;//overflow check for multipliers stage");
		fprintf(outfile,"\nlogic overflow2;//overflow check for output stage");
		fprintf(outfile,"\nlogic overflowfS;//overflow check for output stage");
	// implementation begins here
	// case 1 handles g=0 low pipeline , case 2 handles g=1 ultra pipelined mode

	switch(g)
	{

	case 0:

	//handle special case of p=2 without an extra adder stage
	if (p==2)
	{

		for (i=0;i<p/2;i++)
		{
		fprintf (outfile,"\nassign mul%dreg = in%dreg * in%dreg;",i,i*2,i*2+1);
		}
		for (i=0;i<p/2;i++)
		{

		fprintf(outfile,"\nassign add_out = mul%dreg + freg;",i);

		}
		//overflow condition
		fprintf(outfile,"\nassign overflow = (( (mul0reg[%d]== 1'b1) && (freg[%d] == 1'b1) && (add_out[%d] == 1'b0))||( (mul0reg[%d]== 1'b0) && (freg[%d] == 1'b0) && (add_out[%d] == 1'b1)))? 1'b1 : 1'b0 ;",2*b-1,2*b-1,2*b-1,2*b-1,2*b-1,2*b-1);
		//sequential code here
		fprintf(outfile,"\nalways_ff@(posedge clk)\nbegin\nif(reset)\nbegin");

		for (i=0;i<p;i++)
			{
			fprintf (outfile,"\nin%dreg <= %d'b0;",i,b);
			}// reset ends
			fprintf (outfile,"\nfreg <= %d'b0;",b*2);
			fprintf (outfile,"\nend\nelse if(!reset)\nbegin");
			for (i=0;i<p;i++)
						{
						fprintf (outfile,"\nin%dreg <= in%d;",i,i);
						}
			fprintf (outfile,"\nfreg <= add_out;");
			fprintf (outfile,"\nf <= freg;");
			fprintf (outfile,"\nend");
			fprintf (outfile,"\nend");
			fprintf (outfile,"\nendmodule");

			//testbench begins here
			fprintf(outfile,"\n\n\n");
	}//p==2 ends here

	else
		if (p>=4)
		{

					for (i=0;i<p/2;i++)
					{
					fprintf (outfile,"\nassign mul%dreg = in%dreg * in%dreg;",i,i*2,i*2+1);
					}


					for (i=0;i<p/4;i++)
					{
					fprintf (outfile,"\nassign add_outS%d =",i);
					fprintf (outfile," mul%dreg+mul%dreg;",i*2,i*2+1);
					fprintf(outfile,"\nassign overflowS%d = (( (mul%dreg[%d]== 1'b1) && (mul%dreg[%d] == 1'b1) && (add_outS%d[%d] == 1'b0))||( (mul%dreg[%d]== 1'b0) && (mul%dreg[%d] == 1'b0) && (add_outS%d[%d] == 1'b1)))? 1'b1 : 1'b0 ;",i,i*2,2*b-1,i*2+1,2*b-1,i,2*b-1,i*2,2*b-1,i*2+1,2*b-1,i,2*b-1);

					}
									k=p/2;
									l=0;
									m=0;
									n=0;

									for (j=0;j<(log2(p)-1);j++)
									{

									k=k/2;
									l=l+k;

									for(i=0;i<k/2;i++)
									{

										fprintf (outfile,"\nassign add_outS%d =",i+l);
										fprintf (outfile," add_outS%d+add_outS%d",i*2+m,i*2+1+m);
										fprintf (outfile,";");

											fprintf(outfile,"\nassign overflowS%d = (( (add_outS%d[%d]== 1'b1) && (add_outS%d[%d] == 1'b1) && (add_outS%d[%d] == 1'b0))||( (add_outS%d[%d]== 1'b0) && (add_outS%d[%d] == 1'b0) && (add_outS%d[%d] == 1'b1)))? 1'b1 : 1'b0 ;",i+l,i*2+m,2*b-1,i*2+m+1,2*b-1,i+l,2*b-1,i*2+m,2*b-1,i*2+m+1,2*b-1,i+l,2*b-1);
										n++;
									}
									m=l;

									}
									int v =n+p/4-1;
									//if (p==4)
									//{
									//	fprintf(outfile,"\nassign overflowS%d = (( (add_outS%d[%d]== 1'b1) && (add_outS%d[%d] == 1'b1) && (add_outS%d[%d] == 1'b0))||( (add_outS%d[%d]== 1'b0) && (add_outS%d[%d] == 1'b0) && (add_outS%d[%d] == 1'b1)))? 1'b1 : 1'b0 ;",i+l,i*2+m,2*b-1,i*2+m+1,2*b-1,i+l,2*b-1,i*2+m,2*b-1,i*2+m+1,2*b-1,i+l,2*b-1);
										//v=0;//fprintf (outfile,"\n\n assign add_out = freg + add_outS0;");
									//}
									fprintf (outfile,"\n\n assign add_out = freg + add_outS%d;",v);

					//	printf("heelo n=%d",n+p/2-1);

					fprintf (outfile,"\n\n assign overflowfS = (((add_outS%d[%d] ==1'b1)&&(freg[%d] ==1'b1)&&(add_out[%d] ==1'b0))||((add_outS%d[%d] ==1'b0)&&(freg[%d] ==1'b0)&&(add_out[%d] ==1'b1)));",v,2*b-1,2*b-1,2*b-1,v,2*b-1,2*b-1,2*b-1);

					//overflow logic goes here
					fprintf(outfile,"\nassign overflow = overflowfS");
					for (i=0;i<n+p/4;i++)
					{
					fprintf (outfile,"|| overflowS%d",i);
					}
					fprintf (outfile,";");
			//		fprintf(outfile,"\nassign overflow = overflow1 | overflow2 ;");
					// sequential logic begins here
					// if reset
					fprintf(outfile,"\nalways_ff@(posedge clk)\nbegin\nif(reset)\nbegin");
					for (i=0;i<p;i++)
					{
					fprintf (outfile,"\nin%dreg <= %d'b0;",i,b);
					}// reset ends
					fprintf (outfile,"\nfreg <= %d'b0;",b*2);
					//if not reset
					fprintf (outfile,"\nend\nelse if(!reset)\n begin\n");
					for (i=0;i<p;i++)
					{
					fprintf (outfile,"\nin%dreg <= in%d;",i,i);
					}
					fprintf (outfile,"\nfreg <= add_out;");
					fprintf (outfile,"\nf <= freg;");
					fprintf (outfile,"\n end");
					fprintf (outfile,"\n end\n endmodule");



		}//p>4 ends
	break;








	case 1:// this is the ultra pipelined mode where we have extra registers
	{

		if (p==2)
		{

			for (i=0;i<p/2;i++)
			{
			fprintf (outfile,"\nassign mul%dwire = in%dreg * in%dreg;",i,i*2,i*2+1);
			}
			for (i=0;i<p/2;i++)
			{

			fprintf(outfile,"\nassign add_out = mul%dreg + freg;",i);

			}
			//overflow condition
			fprintf(outfile,"\nassign overflow = (( (mul0reg[%d]== 1'b1) && (freg[%d] == 1'b1) && (add_out[%d] == 1'b0))||( (mul0reg[%d]== 1'b0) && (freg[%d] == 1'b0) && (add_out[%d] == 1'b1)))? 1'b1 : 1'b0 ;",2*b-1,2*b-1,2*b-1,2*b-1,2*b-1,2*b-1);
			//sequential code here
			fprintf(outfile,"\nalways_ff@(posedge clk)\nbegin\nif(reset)\nbegin");

			for (i=0;i<p;i++)
				{
				fprintf (outfile,"\nin%dreg <= %d'b0;",i,b);
				}
				fprintf (outfile,"\nfreg <= %d'b0;",b*2);
				fprintf (outfile,"\nmul0reg <= %d'b0;",b*2);
				// reset ends
				fprintf (outfile,"\nend\nelse if(!reset)\n begin\n");
				for (i=0;i<p;i++)
							{
							fprintf (outfile,"\nin%dreg <= in%d;",i,i);
							}
				for (i=0;i<p/2;i++)
				{
				fprintf (outfile,"\n mul%dreg <= mul%dwire;",i,i);
				}
				fprintf (outfile,"\nfreg <= add_out;");
				fprintf (outfile,"\nf <= freg;");
				fprintf (outfile,"\nend");
				fprintf (outfile,"\nend");
				fprintf (outfile,"\nendmodule");



				//test bench begins here
				fprintf(outfile,"\n\n\n");


		}//p==2 ends here

		else	// implement the design with an adder stage
			if (p>=4)
			{

				for (i=0;i<p/2;i++)
						{
						fprintf (outfile,"\nassign mul%dwire = in%dreg * in%dreg;",i,i*2,i*2+1);
						}
						//fprintf (outfile,"\nassign add_out2 =");
						for (i=0;i<p/4;i++)
						{
						fprintf (outfile,"\nassign add_outS%dwire =",i);
						fprintf (outfile," mul%dreg+mul%dreg;",i*2,i*2+1);
						fprintf(outfile,"\nassign overflowS%d = (( (mul%dreg[%d]== 1'b1) && (mul%dreg[%d] == 1'b1) && (add_outS%dwire[%d] == 1'b0))||( (mul%dreg[%d]== 1'b0) && (mul%dreg[%d] == 1'b0) && (add_outS%dwire[%d] == 1'b1)))? 1'b1 : 1'b0 ;",i,i*2,2*b-1,i*2+1,2*b-1,i,2*b-1,i*2,2*b-1,i*2+1,2*b-1,i,2*b-1);
						}

						k=p/2;
							l=0;
							m=0;
							n=0;

							for (j=0;j<(log2(p)-1);j++)
							{

							k=k/2;
							l=l+k;

							for(i=0;i<k/2;i++)
							{

								fprintf (outfile,"\nassign add_outS%dwire =",i+l);
								fprintf (outfile," add_outS%dreg+add_outS%dreg",i*2+m,i*2+1+m);
								fprintf (outfile,";");
								fprintf(outfile,"\nassign overflowS%d = (( (add_outS%dreg[%d]== 1'b1) && (add_outS%dreg[%d] == 1'b1) && (add_outS%dwire[%d] == 1'b0))||( (add_outS%dreg[%d]== 1'b0) && (add_outS%dreg[%d] == 1'b0) && (add_outS%dwire[%d] == 1'b1)))? 1'b1 : 1'b0 ;",i+l,i*2+m,2*b-1,i*2+m+1,2*b-1,i+l,2*b-1,i*2+m,2*b-1,i*2+m+1,2*b-1,i+l,2*b-1);
								n++;
							}
							m=l;

							}
							int v =n+p/4-1;
							fprintf (outfile,"\nassign add_out = freg + add_outS%dwire;",v);

							//overflow logic goes here
												fprintf(outfile,"\nassign overflow = overflowfS");
												for (i=0;i<n+p/4;i++)
												{
												fprintf (outfile,"|| overflowS%d",i);
												}
												fprintf (outfile,";");
												fprintf (outfile,"\n\n assign overflowfS = (((add_outS%dwire[%d] ==1'b1)&&(freg[%d] ==1'b1)&&(add_out[%d] ==1'b0))||((add_outS%dwire[%d] ==1'b0)&&(freg[%d] ==1'b0)&&(add_out[%d] ==1'b1)));",v,2*b-1,2*b-1,2*b-1,v,2*b-1,2*b-1,2*b-1);
						// sequential logic begins here
						// if reset
						fprintf(outfile,"\nalways_ff@(posedge clk)\nbegin\nif(reset)\nbegin");
						for (i=0;i<p;i++)
						{
						fprintf (outfile,"\nin%dreg <= %d'b0;",i,b);
						}
						for (i=0;i<p/2;i++)
						{
						fprintf (outfile,"\nmul%dreg <= %d'b0;",i,b*2);
						}

						fprintf (outfile,"\nfreg <= %d'b0;",b*2);

						for (i=0;i<v;i++)
						{
						fprintf (outfile,"\nadd_outS%dreg <= %d'b0;",i,b*2);
						}

						// reset ends
						//if not reset
						fprintf (outfile,"\nend\nelse if(!reset)\n begin\n");
						for (i=0;i<p;i++)
						{
						fprintf (outfile,"\nin%dreg <= in%d;",i,i);
						}
						for (i=0;i<p/2;i++)
						{
						fprintf (outfile,"\n mul%dreg <= mul%dwire;",i,i);
						}
						for (i=0;i<v;i++)
						{
						fprintf (outfile,"\nadd_outS%dreg <= add_outS%dwire;",i,i);
						}
						fprintf (outfile,"\nfreg <= add_out;");
						fprintf (outfile,"\nf <= freg;");



						fprintf (outfile,"\n end");
						fprintf (outfile,"\n end\n endmodule");

			}//p>4 ends


		break;
	}
	default: printf("\n Wrong choice of pipelining mode. Now exiting...");
	exit(0);
	}
	fprintf(outfile,"\n\n\n");

			fprintf (outfile,"\nmodule testmodule();");
			fprintf(outfile,"\nlogic clk, reset;");
			fprintf(outfile,"\nlogic signed [%d:0]",b-1);
			for (i=0;i<p;i++)
			{
			fprintf(outfile," in%d",i);
			if(i!=p-1)
			fprintf(outfile,",");
			}
			fprintf(outfile,";\nlogic overflow;");
			fprintf(outfile,"\nlogic signed [%d:0] f;",b*2-1);
			fprintf(outfile,"\ninitial clk = 1;");
			fprintf(outfile,"\ninitial reset = 1;");
			fprintf(outfile,"\nalways #5 clk = ~clk;");
			fprintf(outfile,"\nmac_%d_%d_%d inst1(.clk(clk), .reset(reset),",p,b,g);
			for (i=0;i<p;i++)
			fprintf(outfile," .in%d(in%d),",i,i);
			fprintf(outfile,".overflow(overflow), .f(f));");
			fprintf(outfile,"\ninitial begin\n$monitor($time,, \" ");
			for (i=0;i<p;i++)
			{
			fprintf(outfile,"in%d=%%d",i);
			if(i!=p-1)
			fprintf(outfile,", ");
			}
			fprintf(outfile," f = %%d ,reset = %%d ,overflow = %%d \",");
			for (i=0;i<p;i++)
			{
			fprintf(outfile," in%d,",i);
			}
			fprintf(outfile," f, reset, overflow);");
			fprintf(outfile,"\n@(posedge clk)");
			fprintf(outfile,"\n#10;\nreset =0;");
			for (i=0;i<p;i++)
			{
			fprintf(outfile,"\nin%d=%d;",i,rand()%tb);
			}


			for(loop=0;loop<20;loop++)
						{
						fprintf(outfile,"\n@(posedge clk)");

						fprintf(outfile,"\n#10;");
						for (i=0;i<p;i++)
						{
						fprintf(outfile,"\nin%d=%d;",i,rand()%tb);
						}
						}
			for( loop=0;loop<10;loop++)
					{
					fprintf(outfile,"\n#10;");
					int k =1;
					for (i=0;i<p;i++)
					{
			         k=k*-1;
					fprintf(outfile,"\nin%d=%d;",i,(rand()%tb*k));
					}
					}


			for(loop=0;loop<10;loop++)
			{
			fprintf(outfile,"\n@(posedge clk)");

			fprintf(outfile,"\n#10;");
			for (i=0;i<p;i++)
			{
			fprintf(outfile,"\nin%d=%d;",i,(rand()%tb)+base);
			}
			}
			for( loop=0;loop<10;loop++)
			{
			fprintf(outfile,"\n#10;");
			int k =1;
			for (i=0;i<p;i++)
			{
	         k=k*-1;
			fprintf(outfile,"\nin%d=%d;",i,(rand()%tb+base)*k);
			}
			}
			for(loop=0;loop<20;loop++)
			{
			fprintf(outfile,"\n@(posedge clk)");

			fprintf(outfile,"\n#10;");
			for (i=0;i<p;i++)
			{
			fprintf(outfile,"\nin%d=%d;",i,rand()%tb);
			}
			}
			fprintf(outfile,"\nend\nendmodule");

			printf (" \n Source generation completed ! \n Now exiting...");
			fclose(outfile);
			return 0;
}
