


# I am a comment, and I want to say that the variable CC will be
# the compiler to use.
CC=g++
# Hey!, I am comment number 2. I want to say that CFLAGS will be the
# options I'll pass to the compiler.
CFLAGS=-c -Wall -lm

all: toolgenerator

toolgenerator: toolgen2.o
	$(CC) toolgen2.o -o toolgenerator

toolgen2.o: toolgen2.c
	$(CC) $(CFLAGS) toolgen2.c

clean:
	rm -rf *o hello
